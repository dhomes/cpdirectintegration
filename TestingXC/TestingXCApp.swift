//
//  TestingXCApp.swift
//  TestingXC
//
//  Created by dhomes on 10/22/21.
//

import SwiftUI
import CPCoreKit
import CPCloudKit
import CPLoginKit

@main
struct TestingXCApp: App {
    
    var body: some Scene {
        configureFrameworks()
        return WindowGroup {
            ContentView()
        }
    }
}

func configureFrameworks() {
    
    
    let configuration : CPKeys
    CPToken.autoRefresh = true
    #if DEBUG
    configuration = CPKeysConfiguration(environment: .development,
                                        endpoint: "http://api-dev.liftcommerce.com",
                                        tenant: "FUNDZ",
                                        apikey: "5dbb36169aa4ad00019f74972a304fa29e124044b9702478b24cd3d1",
                                        merchantid: "800000000264",
                                        ccurl: "https://boltgw.cardconnect.com:6443/itoke/ajax-tokenizer.html",
                                        appName: "mytest_app")
    
    let loginItem = CPUserLoginItem(username: "dhomes+01@gmail.com",
                                    password: "x332332x")
    CPLogin().loginUser(loginItem: loginItem) { result in
        switch result {
        case .success(let token):
            print(token)
            // Token would be stored on iOS' Keychain
            if let token = CPToken.currentToken() {
                print(token)
            }
        case .failure(let error):
            print(error)
        }
    }
    
    #else // Production environment, values as provided by CloudPayments
    configuration = CPKeysConfiguration(environment: .production,
                                        endpoint: "",
                                        tenant: "",
                                        apikey: "",
                                        merchantid: "",
                                        ccurl: "",
                                        appName: "")
    #endif
    CloudPayments.setup(configurationKeys: configuration)
}
